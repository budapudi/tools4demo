package example;		
import org.openqa.selenium.By;		
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;		
import org.testng.Assert;		
import org.testng.annotations.Test;	
import org.testng.annotations.BeforeTest;	
import org.testng.annotations.AfterTest;		
public class NewTestDemo {		
	    //private WebDriver driver;		
	    
	
	    public String baseUrl = "https://www.att.com/";
	    String driverPathFirefox = "./src/test/resources/geckodriver.exe"; 
		String driverPathChrome = "./src/test/resources/chromedriver.exe";
		public WebDriver driver;
		@Test				
		public void NewTestDemo() {	
			
			// Step1: Launch att.com
			driver.get(baseUrl);  
			
			// Step2: Get title of att.com
			String title = driver.getTitle();
			
			// Step3: Validate title of att.com page contains "AT&T� Official"
			Assert.assertTrue(title.contains("AT&T� Official")); 		
		}	
		
		// Precondition: Launch browser(chrome/firefox/IE)
		@BeforeTest
		public void beforeTest() {	
		    //driver = new FirefoxDriver();
			//System.out.println("launching firefox browser"); System.setProperty("webdriver.gecko.driver", driverPathFirefox); 
	    	
			System.out.println("launching chrome browser"); 
	    	
			// Setting property file to launch Chrome Driver
	    	System.setProperty("webdriver.chrome.driver", driverPathChrome); 
	    	//driver = new FirefoxDriver(); driver.get(baseUrl); 
	    	
	    	// Initializing driver
	    	driver = new ChromeDriver();
		}	
		
		// Cleanup - closing browser and driver
		@AfterTest
		public void afterTest() {
			driver.quit();			
		}		
}

