package example;		
import org.openqa.selenium.By;		
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;		
import org.testng.Assert;		
import org.testng.annotations.Test;	
import org.testng.annotations.BeforeTest;	
import org.testng.annotations.AfterTest;		
public class NewTest {		
	    //private WebDriver driver;		
	    
	    public String baseUrl = "http://demo.guru99.com/test/newtours/";
	    String driverPathFirefox = "./src/test/resources/geckodriver.exe"; 
		String driverPathChrome = "./src/test/resources/chromedriver.exe";
		public WebDriver driver;
		@Test				
		public void testEasy() {	
			driver.get("https://www.att.com/");  
			String title = driver.getTitle();	
			System.out.println(title);
			Assert.assertTrue(title.contains("AT&T� Official")); 		
		}	
		@BeforeTest
		public void beforeTest() {	
		    //driver = new FirefoxDriver();
			//System.out.println("launching firefox browser"); System.setProperty("webdriver.gecko.driver", driverPathFirefox); 
	    	System.out.println("launching chrome browser"); System.setProperty("webdriver.chrome.driver", driverPathChrome); 
	    	//driver = new FirefoxDriver(); driver.get(baseUrl); 
	    	driver = new ChromeDriver();
		}		
		@AfterTest
		public void afterTest() {
			driver.quit();			
		}		
}

