package test;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

import org.testng.annotations.Test;

import PageFactory.Guru99HomePage;

import PageFactory.Guru99Login;

public class Test99GuruLoginWithPageFactory {

	public String baseUrl = "http://demo.guru99.com/V4/";
    String driverPathFirefox = "C:\\AutomationTools\\geckodriver-v0.21.0-win64\\geckodriver.exe"; 
	String driverPathChrome = "C:\\AutomationTools\\chromedriver_win32_2_35\\chromedriver.exe";
	public WebDriver driver;

    Guru99Login objLogin;

    Guru99HomePage objHomePage;

    

    @BeforeTest

    public void setup(){

    	//driver = new FirefoxDriver();
    	//System.out.println("launching firefox browser"); System.setProperty("webdriver.gecko.driver", driverPathFirefox); 
    	System.out.println("launching chrome browser"); System.setProperty("webdriver.chrome.driver", driverPathChrome); 
    	//driver = new FirefoxDriver(); driver.get(baseUrl); 
    	driver = new ChromeDriver();

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        driver.get(baseUrl);

    }

    /**

     * This test go to http://demo.guru99.com/V4/

     * Verify login page title as guru99 bank

     * Login to application

     * Verify the home page using Dashboard message

     */

    @Test(priority=0)

    public void test_Home_Page_Appear_Correct(){

        //Create Login Page object

    objLogin = new Guru99Login(driver);

    //Verify login page title

    String loginPageTitle = objLogin.getLoginTitle();

    Assert.assertTrue(loginPageTitle.toLowerCase().contains("guru99 bank"));

    //login to application

    objLogin.loginToGuru99("mgr123", "mgr!23");

    // go the next page

    objHomePage = new Guru99HomePage(driver);

    //Verify home page

    Assert.assertTrue(objHomePage.getHomePageDashboardUserName().toLowerCase().contains("manger id : mgr123"));

    }
    
    @AfterTest
   	public void afterTest() {
   		driver.quit();			
   	}	

    

}
